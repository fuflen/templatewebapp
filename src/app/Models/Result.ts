
export class Result<T>{

  data: T;
  hasError: false;
  message: null;
}
