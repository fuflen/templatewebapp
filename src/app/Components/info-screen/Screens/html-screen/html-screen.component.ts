import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-html-screen',
  templateUrl: './html-screen.component.html',
  styleUrls: ['./html-screen.component.css']
})
export class HtmlScreenComponent implements OnInit {
  public _htmlTemplate : string;

  @Input('htmlTemplate')
  set htmlTemplate(template : string){
    this._htmlTemplate = template;
  }


  constructor() { }

  ngOnInit() {
  }

}
