import { Component, OnInit } from '@angular/core';
import {TemplateService} from '../../Services/template.service';

@Component({
  selector: 'app-info-screen',
  templateUrl: './info-screen.component.html',
  styleUrls: ['./info-screen.component.css']
})
export class InfoScreenComponent implements OnInit {

  public htmlTemplate : string = null;
  //public chartData : [];

  constructor(private templateService : TemplateService) { }

  ngOnInit() {
    this.templateService.GetTemplate().subscribe(result => {this.htmlTemplate = result.data.htmlContent; console.log("asefasefase" + result.data.htmlContent)});
  }

}
