import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {safeHtmlPipe} from '../pipes/safeHtmlPipe';
import { InfoScreenComponent } from './Components/info-screen/info-screen.component';
import { HtmlScreenComponent } from './Components/info-screen/Screens/html-screen/html-screen.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TemplateService} from './Services/template.service';

@NgModule({
  declarations: [
    AppComponent,
    safeHtmlPipe,
    InfoScreenComponent,
    HtmlScreenComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    HttpClientModule
  ],
  providers: [HttpClient, TemplateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
